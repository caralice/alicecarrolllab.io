==================================================================
https://keybase.io/alicecarroll
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://blog.alice-carroll.ml
  * I am alicecarroll (https://keybase.io/alicecarroll) on keybase.
  * I have a public key ASA-Bc9gX-KSJlpBLeB4FnyfoVkAsfun_GP_gsRHHfMBGgo

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "01203e05cf605fe292265a412de078167c9fa15900b1fba7fc63ff82c4471df3011a0a",
      "host": "keybase.io",
      "kid": "01203e05cf605fe292265a412de078167c9fa15900b1fba7fc63ff82c4471df3011a0a",
      "uid": "b09f51a87ee9ae51814ca6b12fb99819",
      "username": "alicecarroll"
    },
    "merkle_root": {
      "ctime": 1618512898,
      "hash": "076d2874625c559a817f185033b571df97dfb0153c0da7886191a1ffbff2bd561edb012d25010deeb7b79f63e4e9a8e037492213f52a7cd69d2a693192a61775",
      "hash_meta": "585d90e28b4c3b499c73c6ffea16e1f13509a67e0f2c8479ef3cb75366444bc9",
      "seqno": 19643333
    },
    "service": {
      "entropy": "0eyOlF2Cab5fLihD3AK7UxSG",
      "hostname": "blog.alice-carroll.ml",
      "protocol": "https:"
    },
    "type": "web_service_binding",
    "version": 2
  },
  "client": {
    "name": "keybase.io go client",
    "version": "5.6.1"
  },
  "ctime": 1618512965,
  "expire_in": 504576000,
  "prev": "04bf3c9db43292720e32964f5daf8a239d83f07d28c8bfed85639029e5fc277e",
  "seqno": 10,
  "tag": "signature"
}

which yields the signature:

hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEgPgXPYF/ikiZaQS3geBZ8n6FZALH7p/xj/4LERx3zARoKp3BheWxvYWTESpcCCsQgBL88nbQyknIOMpZPXa+KI52D8H0oyL/thWOQKeX8J37EICBBLp/EZ+5dyyLw2HCXUvHdDwFZY9b/L7UqIte37zorAgHCo3NpZ8RA6aUGODPRe/IGEmekN7VcqTjxGcNoawWO5u3RRhijJN5/wZGN5OFMoqgCjG/gOJZnpauLNYb8CUuBDd9uor5BDahzaWdfdHlwZSCkaGFzaIKkdHlwZQildmFsdWXEINZBtwBAWC37buc/oooZqdz7nBQg3h6VuQulBJpREFuLo3RhZ80CAqd2ZXJzaW9uAQ==

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/alicecarroll

==================================================================
